import * as React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from './login/Login';
import Lockr from 'lockr';
import SelectUserType from './login/SelectUserType';
import AdminRevisor from './revisor/AdminRevisor';
import Navbar from './libs/Navbar';
import Footer from './libs/Footer';
import { getMe } from './libs/users';
import './App.css';
import { AppContext } from './AppContext';
import AdminOperator from './operator/AdminOperator';
import JSGridInitializer from './JSGridInitializer';
import TermsAndCond from './operator/TermsAndCond';
import Terms from './operator/Terms';
import VerifyBilldocs from './operator/VerifyBilldocs';
import UploadBilldoc from './operator/UploadBilldoc';
import UploadTerms from './operator/UploadTerms';
import CompleteParticipantInfo from './operator/CompleteParticipantInfo';

class App extends React.Component {
  setUser = (user) => {
    this.setState({
      user,
    });
  }

  state = {
    user: null,
    setUser: this.setUser,
    isLoading: true,
  }

  componentDidMount() {
    this.loadCurrentUser();
  }

  loadCurrentUser = async () => {
    try {
        const { user, errors, token } = await getMe();
        if (!user) {
          throw Error(errors);
        }
        this.setUser(user);
        Lockr.set('token', token);
        this.setState({
          isLoading: false,
        });
    } catch(ex) {
        this.setState({
          isLoading: false,
        });
        if (window.location.pathname !== '/') {
          window.location.href = '/';
        }
    }
  }

  render() {
    if (this.state.isLoading) {
      return null;
    }
    return (
      <AppContext.Provider value={this.state}>
        <JSGridInitializer />
        <Router>
            <div>
                <Navbar />
                <div className="page-wrapper container-inline">
                    <div className="container-fluid">
                      <Switch>
                        <Route
                          exact
                          path="/"
                          component={Login}
                        />
                        <Route
                          path="/seleccion-tipo-usuario"
                          component={SelectUserType}
                        />
                        <Route
                          path="/terminos-y-condiciones"
                          component={Terms}
                        />
                        <Route
                          path="/admin-revisor"
                          component={AdminRevisor}
                        />
                        <Route
                          path="/admin-operador"
                          component={AdminOperator}
                        />
                        <Route
                          path="/aceptar-terminos"
                          component={TermsAndCond}
                        />
                        <Route
                          path="/verificar-factura"
                          component={VerifyBilldocs}
                        />
                        <Route
                          path="/subir-factura"
                          component={UploadBilldoc}
                        />
                        <Route
                          path="/subir-evidencia-terminos"
                          component={UploadTerms}
                        />
                        <Route
                          path="/completar-informacion"
                          component={CompleteParticipantInfo}
                        />
                      </Switch>
                    </div>
                </div>
                <Footer />
            </div>
        </Router>
      </AppContext.Provider>
    );
  }
}

export default App;
