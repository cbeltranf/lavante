import React, { PureComponent } from 'react';

class JSGridInitializer extends PureComponent {
    componentDidMount() {
        global.jsGrid.setDefaults({
            tableClass: "jsgrid-table table table-striped table-hover"
        });
        global.jsGrid.setDefaults("text", {
            _createTextBox: function() {
                return global.$("<input>").attr("type", "text").attr("class", "form-control input-sm")
            }
        });
        global.jsGrid.setDefaults("number", {
            _createTextBox: function() {
                return global.$("<input>").attr("type", "number").attr("class", "form-control input-sm")
            }
        });
        global.jsGrid.setDefaults("textarea", {
            _createTextBox: function() {
                return global.$("<input>").attr("type", "textarea").attr("class", "form-control")
            }
        });
        global.jsGrid.setDefaults("control", {
            _createGridButton: function(cls, tooltip, clickHandler) {
                var grid = this._grid;
                return global.$("<button>").addClass(this.buttonClass).addClass(cls).attr({
                    type: "button",
                    title: tooltip
                }).on("click", function(e) {
                    clickHandler(grid, e)
                })
            }
        });
        global.jsGrid.setDefaults("select", {
            _createSelect: function() {
                var $result = global.$("<select>").attr("class", "form-control input-sm"),
                    valueField = this.valueField,
                    textField = this.textField,
                    selectedIndex = this.selectedIndex;
                return global.$.each(this.items, function(index, item) {
                    var value = valueField ? item[valueField] : index,
                        text = textField ? item[textField] : item,
                        $option = global.$("<option>").attr("value", value).text(text).appendTo(global.$result);
                    global.$option.prop("selected", selectedIndex === index)
                }), global.$result
            }
        });
    }

    render() {
        return (
            null
        );
    }
}

export default JSGridInitializer;
