import React, { PureComponent } from 'react';
import { PrimaryCard, CardTitle } from '../cards'
import { ButtonSecondaryBlock } from '../buttons';
import config from '../../config';

class AdminRoute extends PureComponent {
  state = {
    auth: this.props.defaultState || config.isTest,
    password: '',
  }

  static defaultProps = {
    validPassword: '1234'
  }

  handlePassValidation = () => {
    if (this.state.password === this.props.validPassword) {
      this.setState({
        auth: true,
      });
    }
  }

  render() {
    if (this.state.auth) {
      return <div>{this.props.children}</div>;
    }

    return (
      <PrimaryCard className="text-center">
        <CardTitle>Ingrese contraseña</CardTitle>
        <form>
          <input
            type="password"
            onChange={(event) => {
              this.setState({
                password: event.target.value,
              });
            }}
            value={this.state.password}
          />
          <ButtonSecondaryBlock onClick={this.handlePassValidation}>Ingresar</ButtonSecondaryBlock>
        </form>
      </PrimaryCard>
    )
  }
}

export default AdminRoute;