import React, { PureComponent } from 'react';
import { listCities } from '../libs/places';

class CitySelect extends PureComponent {
    state = {
        options: [],
    }

    componentDidMount() {
        this.loadData();
    }

    loadData = async () => {
        const { data } = await listCities();
        this.setState({
            options: data,
        });
    }

    render() {
        return (
            <select {...this.props}>
                <option>---</option>
                {this.state.options.map(opt => (
                    <option value={opt.id}>{opt.name} - {opt.departamento.name} </option>
                    ))}
            </select>
        );
    }
}

export default CitySelect;