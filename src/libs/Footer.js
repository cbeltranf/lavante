import React from 'react';

const Footer = () => {
    return (
        <div className="container-fluid">
            <footer className="footer">
                © 2018 Inmov
            </footer>
        </div>
    );
}

export default Footer;