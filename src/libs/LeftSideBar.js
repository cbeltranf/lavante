import React from 'react';

const LeftSideBar = () => {
    return (
        <aside className="left-sidebar">
            <div className="nav-text-box align-items-center d-sm-none">
                <span><img src="/static/assets/images/logo_color.svg" width="100px" alt="elegant admin template" /></span>
                <a className="nav-lock waves-effect waves-dark ml-auto hidden-md-down" href="javascript:void(0)"><i className="mdi mdi-toggle-switch"></i></a>
                <a className="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)"><i className="ti-close"></i></a>
            </div>
            <div className="scroll-sidebar">
                <nav className="sidebar-nav">
                </nav>
            </div>
        </aside>
    );
};
        
export default LeftSideBar;