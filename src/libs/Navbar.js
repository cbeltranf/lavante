import React, { PureComponent } from 'react';
import Topbar from './Topbar';
import LeftSideBar from './LeftSideBar';
import { withRouter } from 'react-router-dom';
import { AppContext } from '../AppContext';

const excludedPaths = ['/'];

class Navbar extends PureComponent {
    static contextType = AppContext;

    render() {
        if (excludedPaths.indexOf(this.props.location.pathname) !== -1) {
            return null;
        }
        return (
            <div>
                <Topbar />
                {this.props.user && <LeftSideBar />}
            </div>
        );
    }
}
                    
export default withRouter(Navbar);
