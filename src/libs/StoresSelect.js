import React, { PureComponent } from 'react';
import { listStores } from '../libs/stores';

class StoresSelect extends PureComponent {
    state = {
        options: [],
    }

    componentDidMount() {
        this.loadData();
    }

    loadData = async () => {
        const { data } = await listStores();
        this.setState({
            options: data,
        });
    }

    render() {
        return (
            <select {...this.props}>
                <option>---</option>
                {this.state.options.map(opt => (
                    <option  value={opt.id}>{opt.name}</option>
                ))}
            </select>
        );
    }
}

export default StoresSelect;
