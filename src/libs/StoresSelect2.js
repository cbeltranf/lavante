import React, { PureComponent } from 'react';
import { listStores } from '../libs/stores';
import Select from 'react-select'

class StoresSelect2 extends PureComponent {
    state = {
        options: [],
    }

    componentDidMount() {
        this.loadData();
    }

    loadData = async () => {
        const { data } = await listStores();

        let items =[];
        data.map(opt => (
            items.push({value: opt.id, label: opt.name})
                  ));

        this.setState({
            options: items,
        });
    }

    render() {
        return (
            <Select options={this.state.options} />
        );
    }
}

export default StoresSelect2;
