import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import { AppContext } from '../AppContext';
import { logout } from '../libs/users'

const excludedPaths = ['seleccion-tipo-usuario'];

class TopBar extends PureComponent {
    static contextType = AppContext;

    handleClick = async () => {
        await logout();
        this.props.history.replace('/')
    }

    render() {
        const isExcluded = excludedPaths.indexOf(this.props.location.pathname) !== -1;
        return (
            <header className="topbar">
                <nav className="navbar top-navbar navbar-expand-md navbar-dark">
                    <div className="navbar-header">
                        <a className="navbar-brand nav-link nav-toggler waves-effect waves-light" >
                            <img src="/logo_lavante.png" style={{
                                maxWidth: '100%',
                                height: '50px',
                                margin: '10px 0',
                            }} alt="homepage" />
                        </a>
                    </div>
                    {this.context.user && (
                        <div className="navbar-collapse">
                            <ul className="navbar-nav mr-auto">
                            </ul>
                            <ul className="navbar-nav my-lg-0 mr-5">
                                <button className="btn btn-primary" onClick={this.handleClick}><i className="mdi mdi-logout" />Salir</button>    
                            </ul>
                        </div>
                    )}
                </nav>
            </header >
        );
    }
}

export default withRouter(TopBar);
