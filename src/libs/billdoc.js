import lockr from 'lockr';

export async function validateBilldoc(invoice, stores_id, customers_id, assignments_id) {
    const token = lockr.get('token');
    const res = await fetch(`${process.env.REACT_APP_HOST}/api/admin/redemption/validate`, {
        method: 'post',
        body: JSON.stringify({ invoice, stores_id, customers_id, assignments_id }),
        headers:{
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`,
        },
    });
    return await res.json();
}