import lockr from 'lockr';

export async function newStore(name) {
    const token = lockr.get('token');
    const res = await fetch(`${process.env.REACT_APP_HOST}/api/admin/store`, {
        method: 'post',
        body: JSON.stringify({ name }),
        headers:{
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`,
        },
    });
    return await res.json();
}