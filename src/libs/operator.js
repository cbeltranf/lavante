import lockr from 'lockr';

export async function getAssignments(operatorId, page = 0) {
    const token = lockr.get('token')
    const data = await fetch(`${process.env.REACT_APP_HOST}/api/admin/assignment?complete=0&user=${operatorId}&page=${page}&pageSize=20000`, {
        method: 'get',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    })
    const res = await data.json()
    return res;
}

export async function cancelAssignment(assignmentId) {
    const token = lockr.get('token')
    const data = await fetch(`${process.env.REACT_APP_HOST}/api/admin/assignment/${assignmentId}`, {
        method: 'delete',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    })
    const res = await data.json()
    return res;
}
