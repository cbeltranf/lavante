import lockr from 'lockr';

export async function registerNewParticipant(mobile) {
    const token = lockr.get('token')
    const body = await fetch(`${process.env.REACT_APP_HOST}/api/admin/assignment`, {
        method: 'post',
        body: JSON.stringify({ mobile }),
        headers:{
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
            'Accept': 'application/json'
        },
    })
    const res = body.json();
    return res;
}

export async function updateRedemption(id, attributes) {
    const token = lockr.get('token')
    const body = await fetch(`${process.env.REACT_APP_HOST}/api/admin/redemption/${id}`, {
        method: 'put',
        body: JSON.stringify(attributes),
        headers:{
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
            'Accept': 'application/json',
        },
    })
    const res = body.json();
    return res;
}