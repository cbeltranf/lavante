import lockr from 'lockr';

export async function listCities() {
    const token = lockr.get('token')
    const res = await fetch(`${process.env.REACT_APP_HOST}/api/admin/city`, {
        method: 'get',
        headers:{
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`,
        },
    });
    return await res.json();
}