import lockr from 'lockr';

export async function listStores() {
    const token = lockr.get('token')
    const res = await fetch(`${process.env.REACT_APP_HOST}/api/admin/store`, {
        method: 'get',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
            'Accept': 'application/json',
        },
    });
    return await res.json();
}