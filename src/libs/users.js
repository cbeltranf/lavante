import Lockr from 'lockr';

export async function authenticate(document, password) {
    const res = await fetch(`${process.env.REACT_APP_HOST}/api/auth/login`, {
        method: 'post',
        body: JSON.stringify({ document, password }),
        headers:{
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    });
    return await res.json();
}

export async function getMe() {
    const token = Lockr.get('token');
    if (!token) {
        throw Error('not token defined');
    }
    const res = await fetch(`${process.env.REACT_APP_HOST}/api/auth/me`, {
        headers:{
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${Lockr.get('token')}`,
            'Accept': 'application/json',
        },
    });
    return await res.json();
}

export async function updateParticipantInfo(id, info) {
    const token = Lockr.get('token')
    const res = await fetch(`${process.env.REACT_APP_HOST}/api/admin/customer/${id}`, {
        method: 'put',
        body: JSON.stringify(info),
        headers:{
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
            'Accept': 'application/json',
        },
    });
    return await res.json();
}


export async function logout() {
    const token = Lockr.get('token')
    const res = await fetch(`${process.env.REACT_APP_HOST}/api/auth/logout`, {
        method: 'get',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
            'Accept': 'application/json',
        },
    });
    return await res.json();
}

export async function updateUser(id, fields) {
    const token = Lockr.get('token');
    const res = await fetch(`${process.env.REACT_APP_HOST}/api/admin/user/${id}`, {
        method: 'put',
        body: JSON.stringify(fields),
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`,
        },
    });
    return await res.json();
}

// 1 receptor
// 2 operador
export async function selectRole(role) {
    const token = Lockr.get('token');
    const res = await fetch(`${process.env.REACT_APP_HOST}/api/admin/session/set/role/${role}`, {
        method: 'get',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`,
        },
    });
    return await res.json();
}
