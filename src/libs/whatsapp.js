export function generateWhatsappLink(phone, text) {
    const phoneClean = phone.replace(/[\s\+]/g, '');
    return `https://api.whatsapp.com/send?phone=${phoneClean}&text=${text}`;
}