import React, { PureComponent } from 'react';
import './login-register-lock.css';
import { authenticate } from '../libs/users';
import styled from 'styled-components';
import background from './background.jpg';
import Lockr from 'lockr';
import { AppContext } from '../AppContext.js';

const Logo = styled.img`
    padding: 0.5em 1em;
    width: 50%;
    margin: 0 auto;
`;

class Login extends PureComponent {
    state = {
        sending: false,
        idn: '',
        password: '',
        errors: {},
    }

    onChangeField = (fieldname) => (event) => {
        this.setState({
            [fieldname]: event.target.value,
        });
    }

    validate = () => {
        const errors = {};
        if (!this.state.idn) {
            errors.idn = 'Campo requerido';
        }
        if (!this.state.password) {
            errors.password = 'Campo requerido';
        }
        return errors;
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const errors = this.validate();
        this.setState({
            errors,
        });

        if (Object.keys(errors).length === 0) {
            try {
                const { accessToken, error, ...userinfo } = await authenticate(this.state.idn, this.state.password);
                if (error) {
                    throw Error(error);
                }
                Lockr.set('token', accessToken);
                Lockr.set('userinfo', userinfo);
                this.context.setUser(userinfo);
                this.props.history.replace('/seleccion-tipo-usuario');
            } catch(ex) {
                console.error(ex);
                alert('Error al iniciar sesión por favor verifique sus credenciales')
            }
        }
    }

    static contextType = AppContext;

    render() {
        return (
            <div className="login-register" style={{
                backgroundImage: `url(${background})`,
                backgroundSize: 'cover',
                position: 'fixed',
                top: 0,
                left: 0,
            }}>
                <div className="login-box card">
                    <div className="card-body">
                        <form className="form-horizontal form-material" id="loginform" onSubmit={this.handleSubmit}>
                            <h3 className="box-title m-b-20 text-center">
                                <Logo src="/logo_lavante.png" />
                            </h3>
                            <div className="form-group ">
                                <div className="col-xs-12">
                                    <input
                                        className="form-control"
                                        type="text"
                                        required=""
                                        name="idn"
                                        placeholder="Número de documento"
                                        onChange={this.onChangeField('idn')}
                                    />
                                    <div>{this.state.errors.idn}</div>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="col-xs-12">
                                    <input
                                        className="form-control"
                                        type="password"
                                        required=""
                                        placeholder="Contraseña"
                                        onChange={this.onChangeField('password')}
                                    />
                                    <div>{this.state.errors.password}</div>
                                </div>
                            </div>
                            <div className="form-group text-center">
                                <div className="col-xs-12 p-b-20">
                                    <button className="btn btn-block btn-lg btn-dark" type="submit">Ingresar.</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;