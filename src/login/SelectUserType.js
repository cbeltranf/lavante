import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../AppContext';
import { selectRole } from '../libs/users';

class SelectUserType extends PureComponent {
    static contextType = AppContext;

    makeCallbackFor = (nextPath) => {
        
    }

    changeRoleHandler = (role) => () => {
        selectRole(role);
    }
    
    render() {
        return (
            <form className="row mt-3">
                <div className="col-12">
                    <div className="card">
                        <div className="card-body">
                            <Link to="/admin-revisor" className="btn btn-lg btn-block btn-dark" onClick={this.changeRoleHandler(1)}>Ingresar como Receptor</Link>
                            <Link to="/admin-operador" className="btn btn-lg btn-block btn-dark" onClick={this.changeRoleHandler(2)}>Ingresar como operador</Link>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}

export default SelectUserType;
