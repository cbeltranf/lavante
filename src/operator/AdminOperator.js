import React, { PureComponent } from 'react';
import moment from 'moment';
import { getAssignments } from '../libs/operator';
import { AppContext } from '../AppContext'; 
import { generateWhatsappLink } from '../libs/whatsapp';
import { updateUser } from '../libs/users';
import swal from 'sweetalert';
require("moment/min/locales.min");


moment.locale('es');

class AdminOperator extends PureComponent {
    state = {
        phone: '',
        current_url: window.location.href,
    }

    componentDidMount() {
        if (this.context.user) {
            this.initializeTable();   
        }
    }

    componentDidUpdate(prevProps) {
        if (this.context.user && !this.state.phone) {
            this.initializeTable();   
        }
    }

    initializeTable = () => {
        this.setState({
            phone: this.context.user.phone,
        });
        const t = this;
        global.$("#jsGrid").jsGrid({
            width: "100%",
            height: "400px",
            sorting: false,
            paging: true,
            autoload: true,
            heading: true,
            pageSize: 5,
            fields: [
                { name: 'customer.mobile', type: "text", title: "Teléfono participante:: ", width: '150px',  align: "center",  },
                { name: 'customer.created_at', type: "date", title: "Fecha de creación::", width: '150px',  align: "center",  },
                { type: "control", title: "Iniciar proceso", width: 100,  align: "center", title: "Acciones", itemTemplate: (value, item) => {
                    var $result = global.jsGrid.fields.control.prototype.itemTemplate.apply(this, {});
                    const link = generateWhatsappLink(item.customer.mobile, `Buen día me llamo ${item.user.name} voy a ser tu asesor, si tienes inquietudes de PLANCHA LOCURA yo puedo resolverlas, por ahora te envío los términos y condiciones. Luego de leerlos debes escribir ACEPTO y enviarlo si estas de acuerdo. https://www.planchalocura.com/terminos-y-condiciones`);
                    const $customButton = global.$(`<a href="${link}" target="_blank" class="btn btn-dark">Empezar</a>`);
                    $customButton.on('click', () => {
                        t.props.history.push(`/aceptar-terminos?phone=${item.customer.mobile}&cid=${item.customer.id}&aid=${item.id}`)
                    });
                    return $result.add($customButton);
                }  },
            ],
            controller:  {
                loadData: async ({ page }) => {
                    try {
                        const { data } = await getAssignments(this.context.user.id, page);
                        const dataWithDate = data.map(d => ({
                            ...d,
                            customer: {
                                ...d.customer,
                                created_at: moment(d.customer.created_at).fromNow(),
                            }
                        }));
                        return dataWithDate;
                    } catch(ex) {
                        swal('Error', 'Error conectando al servidor por favor revise su conexión y luego recargue la página', "error");
                    }
                }
            },
            pagePrevText: 'Anterior',
            pageNextText: 'Siguiente',
            pageFirstText: 'Inicio',
            pageLastText: 'Fin',
            noDataContent: 'No tienes participantes asignados',
        });
        setInterval(function() {
            global.$("#jsGrid").jsGrid("loadData");
        }, 10000);
    }

    updatePhone = (event) => {
        this.setState({
            phone: event.target.value,
        });
    }

    handleUpdate = async () => {
        try {
            const { errors, message, ...user } = await updateUser(this.context.user.id, {
                phone: this.state.phone,
            });
            if (errors) {
                throw new Error(message);
            }
            this.setState({
                phone: user.phone,
            })
            this.context.setUser(user)
            swal("Hecho", 'Se actualizó tu número de contácto', "success");
        } catch(ex) {
            swal("Error", ex.message, "error");
        }
    }

    static contextType = AppContext;

    render() {
        return (
            <div className="card mt-4">
                <div className="card-body">
                    <div className="col">
                        <div className="row justify-content-start">
                            <div className="col">
                                <div className="form-group">
                                    <input
                                        onChange={this.updatePhone}
                                        value={this.state.phone}
                                        className="form-control"
                                        placeholder="Teléfono"
                                    />
                                </div>
                            </div>
                                <div className="col-2 text-right">
                                    <button
                                        className="btn btn-success"
                                        onClick={this.handleUpdate}
                                    >Actualizar</button>
                                </div>
                        </div>
                    </div>
                    <div id="jsGrid" className="col"></div>
                </div>
            </div>
        );
    }
}

export default AdminOperator;
