import React, { PureComponent } from 'react';
import qs from 'qs';
import CitySelect from '../libs/CitySelect';
import { updateParticipantInfo } from '../libs/users';
import { generateWhatsappLink } from '../libs/whatsapp'; 
import LeaveButton from './LeaveButton';

class CompleteParticipantInfo extends PureComponent {
    state = {
        current_url: window.location.href,
        showButton: true
    }

    componentDidCatch(ex) {
        console.log(ex);
    }

    hiddeButton() {
        alert("hi");
        this.setState({ showButton: false });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        
        this.setState({ showButton: false });

        const { phone, rid, aid, cid } = qs.parse(this.props.location.search.replace('?', ''));
        // tener cuidado puesto que esta funcion dispara tambien la redencion de las planchas
        const res = await updateParticipantInfo(cid, {
            "name": this.state.name,
            "lastname": this.state.lastname,
            "mobile": phone,
            "document": this.state.idn,
            "cities_id": this.state.city,
            "redemptons_id": rid,
            "assigments_id": aid,
            "adress": this.state.address,
            "comments": this.state.comments
        });
        
        if (res.errors) {
            return alert("Error redimiendo plancha es posible que no exista stock disponible")
        }
        
        this.setState({
            servientrega: res.servientrega,
        })

    }

    makeChangeHandler = (fieldname) => (event) => {
        this.setState({
            [fieldname]: event.target.value,
        });
    }

    generateWhatsappText = () => {
        var tmp = this.state.servientrega.Fecha_Limite.date.split(" ");
        const redemDate = tmp[0]; //new Date(this.state.servientrega.Fecha_Limite.date);
        tmp = this.state.servientrega.Fecha_Inicio.date.split(" ");
        const initDate =  tmp[0];//new Date(this.state.servientrega.Fecha_Inicio.date);
//return `¡Validación exitosa! Recuerda que para reclamar tu plancha REMINGTON debes llevar a la sede de Servientrega  ${this.state.servientrega.Sede} - ${this.state.servientrega.Direccion}  tu cedula, el pin (${this.state.servientrega.Guia}) y $ ${this.state.servientrega.Precio} pesos en efectivo. No olvides que debes esperar 5 días hábiles para reclamar tu plancha. (Desde el ${initDate.getDay()}/${initDate.getMonth()}/${initDate.getFullYear()} y antes del ${redemDate.getDay()}/${redemDate.getMonth()}/${redemDate.getFullYear()}).  Gracias por ser parte de L´avanté Paris.`;
        return `¡Validación exitosa! Recuerda que para reclamar tu plancha REMINGTON debes llevar a la sede de Servientrega  ${this.state.servientrega.Sede} - ${this.state.servientrega.Direccion}  tu cedula, el pin (${this.state.servientrega.Guia}) y $ ${this.state.servientrega.Precio} pesos en efectivo. No olvides que debes esperar 5 días hábiles para reclamar tu plancha. (Desde el ${initDate} y antes del ${redemDate}).  Gracias por ser parte de L´avanté Paris.`;
    }

    render() {
    
        const { phone } = qs.parse(this.props.location.search.replace('?', ''));
        return (
            <div>
                <div className="row page-titles">
                    <div className="col-md-5 align-self-center">
                        <h4 className="text-themecolor">Completar información</h4>
                    </div>
                </div>
                <div className="card">
                    <div className="card-body">
                        {!this.state.servientrega && (
                            <form onSubmit={this.handleSubmit} action="#">
                                <div className="form-group">
                                    <label>Nombres.:</label>
                                    <input
                                        required
                                        className="form-control"
                                        value={this.state.name}
                                        onChange={this.makeChangeHandler('name')}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Apellidos.:</label>
                                    <input
                                        required
                                        className="form-control"
                                        value={this.state.lastname}
                                        onChange={this.makeChangeHandler('lastname')}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Dirección:</label>
                                    <input
                                        required
                                        className="form-control"
                                        value={this.state.address}
                                        onChange={this.makeChangeHandler('address')}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Ciudad:</label>
                                    <CitySelect
                                        onChange={this.makeChangeHandler('city')}
                                        value={this.state.city}
                                        required
                                        className="form-control"
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Número de identificación:</label>
                                    <input type="number"
                                        required
                                        className="form-control"
                                        value={this.state.idn}
                                        onChange={this.makeChangeHandler('idn')}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Comentarios:</label>
                                    <textarea
                                        required
                                        className="form-control"
                                        value={this.state.comments}
                                        onChange={this.makeChangeHandler('comments')}
                                    />
                                </div>
                                <div className="mt-2">
                                    <LeaveButton></LeaveButton>
                                    <button  style={{display: this.state.showButton ? 'inline-block' : 'none' }} className="btn btn-dark">Verificar </button>
                                </div>
                            </form>
                        ) || (
                            <div>
                                <div>
                                    <label className="card-text font-weight-bold">Guia:  </label>
                                    <label className="card-text">{this.state.servientrega.Guia}</label>
                                </div>
                                <div>
                                    <label className="card-text font-weight-bold">Sede Servientrega (CDS): </label>
                                    <label className="card-text">{this.state.servientrega.Sede}</label>
                                </div>
                                <a
                                    className="btn btn-success"
                                    href={generateWhatsappLink(phone, this.generateWhatsappText())}
                                    target="_blank"
                                    tabIndex="0" 
                                    onClick={() => this.props.history.replace(`/admin-operador`)}
                                >Enviar instrucciones</a>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default CompleteParticipantInfo;
