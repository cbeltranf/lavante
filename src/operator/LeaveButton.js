import React, { PureComponent } from 'react';
import qs from 'qs';
import { withRouter } from 'react-router-dom';
import swal from 'sweetalert';
import { cancelAssignment } from '../libs/operator';
import { generateWhatsappLink } from '../libs/whatsapp';


class LeaveButton extends PureComponent {
    state = {}

    leave = () => {
        this.props.history.push('/admin-operador')
    }

    confirm = async () => {
        const { phone, aid } = qs.parse(this.props.location.search.replace('?', ''));
        const willReturn = await swal({
            title: "confirmación",
            text: "Esta seguro que desea abandonar el proceso?",
            icon: "warning",
            buttons: {
                cancel: "Cancelar",
                confirm: 'Confirmar',
            },
            dangerMode: true,
        });
        

        if (willReturn) {
            await cancelAssignment(aid);
            window.open(
                generateWhatsappLink(phone, 'No ha sido posible completar el proceso'),
                '_blank'
            )
            this.props.history.push('/admin-operador');
        }
    }

    render() {
        return (
            <a tabindex="0" className="btn btn-danger mr-2" onClick={this.confirm}>
                Salir
            </a>          
        );
    }
}

export default withRouter(LeaveButton);