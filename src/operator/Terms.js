import React, { PureComponent } from 'react';

class Terms extends PureComponent {
    state = { 
        current_url: window.location.href,
     }
    render() {
        return (
           <div className="card mt-5">
                <div className="card-body">
               <h2 className="mb-4">TÉRMINOS Y CONDICIONES PROMOCIONAL “PLANCHA LOCURA”</h2>
               <p>
                Entiéndase por términos y condiciones de la actividad promocional <strong>“PLANCHA LOCURA L’AVANTÉ”</strong> (en adelante la Actividad), todos aquellos requisitos y restricciones que se deben tener en cuenta para participar en la Actividad y ser beneficiario de la misma. Se aclara expresamente que toda persona que decida participar en la Actividad se sujeta a los términos y condiciones aquí señaladas, por ello los usuarios que participen en la Actividad promocional aceptan sin ninguna modificación y/o restricción, las condiciones, términos y avisos contenidos en este documento
               </p>
               <p>
                Promoción aplica para compras en establecimiento de comercio a nivel Nacional que genere factura de venta con la información de compras superiores a $50.000 en los productos de L'AVANTÉ PARIS®, de las siguientes marcas participantes: MAXYLASH® - RENACELL® - REGENEXT® - AQUA24® - INTIBON® - FIGUBELLE® - CLEANCUTIX® - FOTONE® - VOLUMIA®. Válido por compras realizadas entre el 01 de Noviembre al 24 de Diciembre de 2018 o hasta agotar existencias, lo primero que se presente. En total son 10.000 planchas para el cabello marca REMINGTON® de las siguientes referencias: 3.439 unds ref:S1300(BV)F; 2.919 unds ref:S1300(BV)F; 1.640 unds ref:S1005(BV)FF; 1.292 unds ref:S7300(110)F; 710 unds ref:S5500E. Para reclamar la Plancha debe seguir los pasos escribiendo por WhatsApp a la línea 3208695205. La entrega de las Planchas se realizará en los puntos de servicio autorizados indicados de SERVIENTREGA®. Ver términos, condiciones y mecánica detallada en la página web: <a href="www.planchalocura.com"></a>
               </p>
                <strong>VIGENCIA:</strong> La Actividad estará vigente desde el día 1 de noviembre hasta el día 24 de diciembre de 2018, o hasta agotar existencias, lo primero que ocurra primero.
               <p>
                <strong>PREMIOS:</strong> En total son 10.000 planchas para el cabello marca REMINGTON de las siguientes referencias: 3.439 unds ref:S1300(BV)F; 2.919 unds ref:S1300(BV)F; 1.640 unds ref:S1005(BV)FF; 1.292 unds ref:S7300(110)F; 710 unds ref:S5500E
               </p>
               <p>
                <strong>TERRITORIO:</strong> La Actividad tendrá validez para todo el territorio de Colombia, por compras realizadas en establecimientos comerciales y en plataformas digitales, con la presentación de la factura de compra legal.
               </p>
               <p>
                <strong>MECÁNICA DE LA ACTIVIDAD:</strong> Para obtener una (1) plancha para el cabello se deberá cumplir el siguiente proceso:
                <ol>
                    <li>El Participante debe comprar productos de L´AVANTÉ PARIS®. Marcas participantes: MAXYLASH® - RENACELL® - REGENEXT® - AQUA24® - INTIBON® - FIGUBELLE® - CLEANCUTIX® - FOTONE® - VOLUMIA®), por valor superior o igual a $50.000 y conservar la factura. La facturas no son acumulables.</li>
                    <li>El Consumidor debe enviar vía whatsapp la palabra “PLANCHA LOCURA” al número Celular 3208695205</li>
                    <li>Un asesor de L´AVANTÉ se comunicará vía Whatsapp con el participante, el cual le dará la bienvenida y le enviará los términos y condiciones de la actividad, el participante debe leer y escribir ACEPTAR, para seguir con el proceso.</li>
                    <li>El Participante debe enviar vía Whatsapp la foto de la factura de compra, número de la cédula de ciudadanía, nombre completo, dirección de residencia, Barrio, Ciudad.</li>
                    <li>Conforme la dirección reportada por el Participante, se le indicará el punto SERVIENTREGA más cercano donde puede reclamar la plancha de REMINGTON para el cabello con un número de comprobante que enviará el asesor de L´AVANTÉ por Whatsapp.</li>
                    <li>Para participar aplica las siguientes condiciones: Debe realizar compras de Productos de L’AVANTÉ PARIS mayores a $50.000. Los productos deben estar debidamente registrados detallados en la factura. La foto de la factura debe ser legible, que indique el número de la factura, la fecha de compra y los productos de L’AVANTÉ PARIS, la factura no debe tener tachones ni enmendaduras. Máximo se puede redimir 10 Planchas por cédula y por factura. La factura de compra será inhabilitada luego de quedar registrada en el sistema. Aplica para compras realizadas entre el 01 de Noviembre al 24 de Diciembre de 2018 en cualquier establecimiento comercial o plataforma digital, que genere factura física legal.</li>
                    <li>Proceso de reclamo de la Plancha REMINGTON en Puntos Servientrega: El Participante tendrá 8 días calendario para reclamar la Plancha en el Punto Servientrega Indicado. Se le informará por medio de mensaje de texto al número celular registrado cuando ya esté disponible la Plancha en el Punto Servientrega indicado. <strong>Para reclamar la Plancha en el Punto SERVIENTREGA indicado, el Participante debe llevar factura o copia de la factura de compra, la cédula de ciudadanía, $50.000 en Efectivo y el número de comprobante o código indicado por el Asesor de L’AVANTÉ PARIS por medio de WhatsApp.</strong></li>
                    <li>La fecha máxima para participar en la actividad enviando el mensaje “PLANCHA LOCURA” al celular 3208695205, es el 28 de Diciembre de 2018 y la fecha máxima para reclamar las planchas es el 10 de Enero de 2019.</li>
                </ol>
               </p>
               <h2>GENERALIDADES</h2>
               <h3>1. Participantes</h3>
               <p>
                    A. Pueden participar de la actividad promocional las personas mayores de edad, residentes en Colombia. Se encuentra prohibida la participación de los empleados directos y/o bajo cualquier otra forma de vinculación de Biotecnik S.A.S y de empresas vinculadas a esta actividad. 
                    B. Los participantes se hacen responsables de la veracidad de la información proporcionada al organizador de la actividad al momento de ejecutar la mecánica de la actividad y de la entrega de la plancha para el cabello marca REMINGTON.
               </p>
               <h3>2. Facturas a presentar:</h3>
               <p>
                    Las facturas a presentar deben estar completas, en buen estado y completamente legibles. Únicamente aplican facturas de compra con fechas del 01 de Noviembre al 24 de Diciembre de 2018. No aplica para facturas acumulables. El valor de los $50.000 en compras de Productos de L’AVANTÉ PARIS, debe estar registrada en una misma factura.
               </p>
               <h3>3. Referencias de Planchas: </h3>
               <p>
                    Se tendrá inventario de 10.000 Planchas marca REMINGTON en 5 referencias asi: 3.439 unds ref:S1300(BV)F; 2.919 unds ref:S1300(BV)F; 1.640 unds ref:S1005(BV)FF; 1.292 unds ref:S7300(110)F; 710 unds ref:S5500E. No se tiene la opción que el Participante seleccione la referencia. Las planchas se irán entregando conforme el inventario entregado por REMINGTON y que tendremos en la Bodega de SERVIENTREGA. El Precio regular promedio en el mercado de las Planchas REMINGTON que trabajaremos en la Actividad, oscilan entre $120.000 y $190.000. La imagen de referencia de la plancha que se tiene en la publicidad corresponde a la plancha más económica de todas las referencias
               </p>
               <h3>4. Garantía Planchas REMINGTON: </h3>
               <p>
               Las planchas entregadas tienen garantía de 2 años, cualquier tipo de reclamo de Calidad de las Planchas REMINGTON, el participante debe realizar el siguiente proceso directamente con REMINGTON – SPECTRUM BRANDS: 
               <p>
                    A. Comuníquese con la línea de Servicio al cliente de REMINGTON: 
                    LINEA DE SERVICIO AL CLIENTE NACIONAL: 018000510012. 
                    EN BOGOTA: 6231310
               </p>
                <p>
                    B. Indicar Fecha cuando reclamo la Plancha en Servientrega, indicar los datos completos del cliente, con los siguientes requisitos:
                    <ul>
                        <li>Nombre completo del cliente. </li>
                        <li>Cedula. </li>
                        <li>Referencia Producto. </li>
                        <li>Departamento </li>
                        <li>Ciudad </li>
                        <li>Teléfono fijo. </li>
                        <li>Celular. </li>
                        <li>Lugar de Compra. </li>
                        <li>Fecha de compra. </li>
                        <li>Motivo de la Solicitud. </li>
                    </ul>
                </p>
                <p>
                    C. En caso de no lograr comunicación con la línea notificar el caso incluyendo los requisitos a: 
                    Nayibe.pedraza@la.Spectrumbrands.com 
                    servicioalcliente@la.Spectrumbrands.com 
                    * Anexar video de la falla o síntoma reportado donde se evidencia. 
                </p>
                <p>
                    D. Revisado el caso se notifica el paso a seguir: 
                    a. Si es cambio se envía producto nuevo a la casa del cliente o al 
                    almacén. 
                    b. Si es cambio con carta, cliente entrega carta con el producto 
                    al almacén para hacer el cambio desde el almacén. 
                    c. Si el producto no falla se retorna al cliente. 
                    Nota: en caso de reincidencia se podrá recolectar por la marca para verificar la falla indicada.  

                    L'AVANTÉ PARIS ®, son productos dermocosméticos, elaborados en Colombia, para BIOTECNIK S.A.S., con tecnología y asistencia de Laboratorios SEDERMA (Paris Francia). www.lavante-paris.com
                </p>

               </p>
               </div>
           </div> 
        );
    }
}

export default Terms;