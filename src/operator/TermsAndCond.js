import React, { PureComponent } from 'react';
import qs from 'qs';
import { generateWhatsappLink } from '../libs/whatsapp';
import { cancelAssignment } from '../libs/operator';

class TermsAndCond extends PureComponent {
    render() {
        const { phone, aid } = qs.parse(this.props.location.search.replace('?', ''));
        return (
            <div>
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Aceptar terminos y condiciones</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div className="card-text">¿El usuario aceptó los terminos y condiciones?</div>
                                <div className="mt-3">
                                    <a target="_blank" onClick={() => {
                                        this.props.history.replace('/admin-operador');
                                        cancelAssignment(aid)
                                    }} href={generateWhatsappLink(phone, 'Si cambias de opinión, acá te esperamos.')} className="btn btn-danger">No aceptó</a>
                                    <a target="_blank" tabIndex="0" onClick={() => this.props.history.replace(`/subir-evidencia-terminos${this.props.location.search}`)} className="btn btn-success ml-2">Si aceptó</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TermsAndCond;
