import React, { PureComponent } from 'react';
import qs from 'qs';
import lockr from 'lockr';
import { updateRedemption } from '../libs/participant';
import LeaveButton from './LeaveButton';
import { generateWhatsappLink } from '../libs/whatsapp'; 
import swal from 'sweetalert';

class UploadBilldoc extends PureComponent {
    state = {
        quantity: 1,
        current_url: window.location.href,
    }
  

    componentDidMount() {
        const token = lockr.get('token');
        const { cid, rid } = qs.parse(this.props.location.search.replace('?', ''));
                
        new global.Dropzone("#files", {
            url: `${process.env.REACT_APP_HOST}/api/admin/redemption/invoice/photo/${rid}`,
            paramName: 'photo',
            headers:{
                'Authorization': `Bearer ${token}`,
            },
            dictDefaultMessage: 'Arrastra las imagenes aquí para subir o has clic',
        });
    }

    componentDidCatch(ex) {
        console.log(ex);
    }

    handleSubmit  = async (event) => {
        event.preventDefault();
        if(this.state.quantity > 0 && this.state.amount > 0){

            if( this.state.amount < 50000){
                swal('Error', 'El valor de la factura debe ser superior a 50.000', "error");
                return false;
            }

            var limit = this.state.amount / 50000;
            limit = Math.floor(limit);
    
            if(this.state.quantity > limit){
                swal('Error', 'La cantidad solicitada no puede ser cubierta por el valor de la factura', "error");
                return false;
            }

        }else {
            swal('Error', 'Debes indicar el precio de la factura y la cantidad de planchas a redimir', "error");
            return false;
        }

        const { cid, rid, phone } = qs.parse(this.props.location.search.replace('?', ''));

        await updateRedemption(rid, {
            quantity: this.state.quantity,
            amount: this.state.amount,
        });
       
        window.open(generateWhatsappLink(phone, this.generateWhatsappText()), '_blank' );


        this.props.history.push(`/completar-informacion${this.props.location.search}`)
    }

    makeChangeHandler = (fieldname) => (event) => {

        this.setState({
            [fieldname]: event.target.value,
        });
    }

    
    generateWhatsappText = () => {

        return `FACTURA VALIDADA, ayúdanos con el envío de los siguientes datos: Nombre completo, Cédula, Dirección, Barrio, Ciudad. En un momento te daremos tu pin y punto mas cercano de servientrega para la entrega de tu plancha.`;
    }

    render() {


        return (
            <div>
                <div className="row page-titles">
                    <div className="col-md-5 align-self-center">
                        <h4 className="text-themecolor">Verificar datos de facturación</h4>
                    </div>
                </div>
                <div className="card">
                    <div className="card-body">
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <label>Valor total de los productos LAVANTÉ en factura:</label>
                                <input
                                    className="form-control"
                                    value={this.state.amount}
                                    required
                                    onChange={this.makeChangeHandler('amount')}
                                />
                            </div>
                            <div className="form-group">
                                <label>Cantidad de planchas a redimir:</label>
                                <input
                                    className="form-control"
                                    value={this.state.quantity}
                                    required
                                    onChange={this.makeChangeHandler('quantity')}
                                />
                            </div>
                            <div className="form-group">
                                <label>Fotografías de las facturas:</label>
                                <div id="files" className="dropzone"></div>
                            </div>
                            <div className="mt-2">
                                <LeaveButton />
                              
                                <button  className="btn btn-dark">Continuar</button>
                          

                                


                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default UploadBilldoc;
