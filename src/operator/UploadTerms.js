import React, { PureComponent } from 'react';
import lockr from 'lockr';
import qs from 'qs';
import { generateWhatsappLink } from '../libs/whatsapp';
import LeaveButton from './LeaveButton';

class UploadBilldoc extends PureComponent {
    state = { 
        current_url: window.location.href,
     }

    componentDidMount() {
        const token = lockr.get('token');
        const { cid } = qs.parse(this.props.location.search.replace('?', ''));
        const myDropzone = new global.Dropzone("#files", {
            url: `${process.env.REACT_APP_HOST}/api/admin/redemption/terms/${cid}`,
            paramName: 'photo',
            maxFiles: 1,
            headers:{
                'Authorization': `Bearer ${token}`,
            },
            maxfilesexceeded: () => {
                alert('Solo puedes subir una captura');
            },
            init: function() {
                this.on("addedfile", function() {
                    if (this.files[1]!=null){
                    this.removeFile(this.files[0]);
                    }
                });
            },
            dictDefaultMessage: 'Arrastra la imagene para subir o has clic aquí',
        });
    }

    componentDidCatch(ex) {
        console.log(ex);
    }

    handleSubmit  = () => {
    }
    
    handleNext  = () => {
        const { cid, phone } = qs.parse(this.props.location.search.replace('?', ''));
        this.props.history.replace(`/verificar-factura${this.props.location.search}`);
    }

    makeChangeHandler = (fieldname) => (event) => {
        this.setState({
            [fieldname]: event.target.value,
        });
    }

    render() {
        const { phone, cid } = qs.parse(this.props.location.search.replace('?', ''));
        return (
            <div>
                <div className="row page-titles">
                    <div className="col-md-5 align-self-center">
                        <h4 className="text-themecolor">Subir captura de aceptación de terminos</h4>
                    </div>
                </div>
                <div className="card">
                    <div className="card-body">
                        <div onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <div id="files" className="dropzone"></div>
                            </div>
                            <div className="mt-2">
                                <LeaveButton />
                                <a target="_blank" onClick={this.handleNext} href={generateWhatsappLink(phone, 'Bienvenido, para continuar con el proceso debes enviar a este chat la foto de la factura de compra.')} className="btn btn-dark">Continuar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UploadBilldoc;