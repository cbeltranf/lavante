import React, { PureComponent } from 'react';
import qs from 'qs';
import { validateBilldoc } from '../libs/billdoc';
import { newStore } from '../libs/newStore';

import StoresSelect from '../libs/StoresSelect';
import LeaveButton from './LeaveButton';
import swalert from 'sweetalert';


class VerifyBillDocs extends PureComponent {
 constructor(props, context) {
        super(props, context);
   this.state = {
            isHidden: true,
            current_url: window.location.href,
        };
      }

      toggleHidden () {
        this.setState({
          isHidden: !this.state.isHidden
        })
      }

      newStoreSubmit = async (event) => {
        event.preventDefault();        
        console.log("Ins");
        this.makeChangeHandler('new_store');
        if(!this.state.new_store ){
           alert('Debes ingresar un nombre para el almacen'); 
        }
        try {
            const { errors, ...data } = await newStore(
                this.state.new_store
            );
            if (errors) {
                this.setState({
                    error: errors.message,
                });
            }
            this.state.store = data.id;
            this.toggleHidden.bind();
            alert('Almacen '+data.name+' creado con éxito.');

        } catch(ex) { 
            alert('El almacen ya se encuentra registrado en el sistema');
        }


      }

    makeChangeHandler = (fieldname) => (event) => {
        this.setState({
            [fieldname]: event.target.value,
        });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const { cid, aid } = qs.parse(this.props.location.search.replace('?', ''));
        try {
            const { errors, ...data } = await validateBilldoc(
                this.state.billdoc_number,
                this.state.store,
                cid,
                aid,
            );
            if (errors) {
                this.setState({
                    error: errors.message,
                });
            }
            this.props.history.replace(`/subir-factura${this.props.location.search}&rid=${data[0].id}`);
        } catch(ex) {
            swalert({
                icon: 'warning',
                text: 'Error con la factura',
                dangerMode: true,
            });
        }
    }

    render() {

        const mybutton = {
            marginTop: '10px',
          };

          

        return (
            <div>
                <div className="row page-titles">
                    <div className="col-md-5 align-self-center">
                        <h4 className="text-themecolor">Verificar datos de facturación</h4>
                    </div>
                </div>
                <div className="card">
                    <div className="card-body">
                        <form onSubmit={this.handleSubmit} action="#">
                            <div className="form-group">
                                <label>Número de factura</label>
                                <input
                                    required
                                    className="form-control"
                                    value={this.state.billdoc_number}
                                    onChange={this.makeChangeHandler('billdoc_number')}
                                />
                            </div>
                            <div className="form-group">
                                <label>Almacén</label>

                              

                                

                                {!this.state.isHidden && (
                                   <div style={ mybutton }>
                                    <input class="form-control" type="text" name="new_store" id="new_store"  onChange={this.makeChangeHandler('new_store')} placeholder="Ingrese el nuevo almacen"/>
                                    <button  
                                    onClick={(event)=>{ this.newStoreSubmit(event); this.toggleHidden(event);}}  
                                    type="button" style={ mybutton } class="btn btn-primary">
                                        <i class="mdi mdi-plus"></i>
                                        Agregar
                                    </button>
                                    </div>
                                ) || (
                                    <div>
                                    <StoresSelect
                                    required
                                    className="form-control"
                                    value={this.state.store}
                                    onChange={this.makeChangeHandler('store')}
                                    />
                                <button  onClick={this.toggleHidden.bind(this)}  type="button" style={ mybutton } class="btn btn-primary">
                                    <i class="mdi mdi-plus"></i>
                                </button>
                                    </div>
                                )}

                               



                            


                                
                            </div>
                            <div className="mt-2">
                                <LeaveButton />
                               
                                <button className="btn btn-dark">Verificar</button>
                               
                            </div>
                        </form>
                        {this.state.error && <div className="alert alert-danger mt-3">
                            {this.state.error}
                        </div>}
                    </div>
                </div>

            </div>
        );
    }
}

export default VerifyBillDocs;