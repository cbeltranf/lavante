import React, { PureComponent } from 'react';
import qs from 'qs';
import { validateBilldoc } from '../libs/billdoc';
import StoresSelect from '../libs/StoresSelect';
import LeaveButton from './LeaveButton';
import swalert from 'sweetalert';

class VerifyBillDocs extends PureComponent {
    state = {
        current_url: window.location.href,
    }

    makeChangeHandler = (fieldname) => (event) => {
        this.setState({
            [fieldname]: event.target.value,
        });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const { cid, aid } = qs.parse(this.props.location.search.replace('?', ''));
        try {
            const { errors, ...data } = await validateBilldoc(
                this.state.billdoc_number,
                this.state.store,
                cid,
                aid,
            );
            if (errors) {
                this.setState({
                    error: errors.message,
                });
            }
            this.props.history.replace(`/subir-factura${this.props.location.search}&rid=${data[0].id}`);
        } catch(ex) {
            swalert({
                icon: 'warning',
                text: 'Error con la factura',
                dangerMode: true,
            });
        }
    }

    render() {
        return (
            <div>
                <div className="row page-titles">
                    <div className="col-md-5 align-self-center">
                        <h4 className="text-themecolor">Verificar datos de facturación</h4>
                    </div>
                </div>
                <div className="card">
                    <div className="card-body">
                        <form onSubmit={this.handleSubmit} action="#">
                            <div className="form-group">
                                <label>Número de factura</label>
                                <input
                                    required
                                    className="form-control"
                                    value={this.state.billdoc_number}
                                    onChange={this.makeChangeHandler('billdoc_number')}
                                />
                            </div>
                            <div className="form-group">
                                <label>Almacén</label>
                                <StoresSelect
                                    required
                                    className="form-control"
                                    value={this.state.store}
                                    onChange={this.makeChangeHandler('store')}
                                />
                            </div>
                            <div className="mt-2">
                                <LeaveButton />
                                <button className="btn btn-dark">Verificar</button>
                            </div>
                        </form>
                        {this.state.error && <div className="alert alert-danger mt-3">
                            {this.state.error}
                        </div>}
                    </div>
                </div>
            </div>
        );
    }
}

export default VerifyBillDocs;