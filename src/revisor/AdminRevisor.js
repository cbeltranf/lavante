import React, { PureComponent } from 'react';
import { registerNewParticipant } from '../libs/participant';

const initialState = {
    processed: false,
    operator: {},
    number: '',
    participantPhoneFormatted: '',
    errors: null,
};

class AdminRevisor extends PureComponent {
    state = initialState

    handleSubmit = async (event) => {
        event.preventDefault();
        try {
            const { operator, participantPhoneFormatted, errors, message } = await registerNewParticipant(this.state.number);
    
            this.setState({
                operator,
                participantPhoneFormatted,
                processed: !errors,
                errors,
                message
            });
        } catch(ex) {
            alert('Error conectando al servidor');
        }

    }

    handleChangeNumber = (event) => {
        this.setState({
            number: event.target.value,
        });
    }

    restartView = () => {
        this.setState(initialState)
    }

    render() {
        const { participantPhoneFormatted = '', operator = {} } = this.state; 

        const messageHtml = <div>L´avanté Paris te da la bienvenida a la Super Promoción PLANCHA LOCURA, nos emociona que participes, a continuación el asesor <strong>{operator.name}</strong> te contactará desde el numero <strong>{operator.phone}</strong> para ser tu guía.</div>;
        const message = `L´avanté Paris te da la bienvenida a la Super Promoción PLANCHA LOCURA, nos emociona que participes, a continuación el asesor ${operator.name} te contactará desde el numero ${operator.phone} para ser tu guía.`;
        const participantPhoneFormattedClean = participantPhoneFormatted.replace(/[\s\+]/g, '');
        return (
            <div>
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Registrar nuevo participante</h4>
                    </div>
                </div>
                <div className="card">
                    <div className="card-body">
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <label>Número célular:</label>
                                <input
                                    className="form-control"
                                    type="tel"
                                    required
                                    onChange={this.handleChangeNumber}
                                    value={this.state.number}
                                />
                            </div>
                            {!this.state.processed && <button className="btn btn-dark btn-block">Registrar</button>}
                            {this.state.processed && (
                                <div>
                                    {messageHtml}
                                    <div>
                                        <a
                                            onClick={this.restartView}
                                            target="_blank"
                                            class="btn btn-success mt-3"
                                            href={`https://api.whatsapp.com/send?phone=${participantPhoneFormattedClean}&text=${message}`}
                                        ><i className="mdi mdi-whatsapp mr-1"/>Enviar mensaje</a>
                                    </div>
                                </div>
                            )}
                            {this.state.errors && (
                                <div className="alert alert-danger mt-3">
                                    {this.state.errors.message && <span>{this.state.errors.message}</span>}
                                    {!this.state.errors.message && <span>{this.state.message}</span>}
                                </div>
                            )}
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default AdminRevisor;
